<?php

namespace App\Services;


use App\Model\Product;

use DB;

class ProductLogic
{

    public static function search_products($name)
    {

        $key = explode(' ', $name);
        
        $products = Product::where(function ($q) use ($key) {
                foreach ($key as $value) {
                    $q->orWhere('products.name', 'like', "%{$value}%");
                }
            })
            ->orderBy('products.name', 'desc');

        return [
            'total_size' => $products->total(),
            'products' => $products->items()
        ];
    }
    
}
