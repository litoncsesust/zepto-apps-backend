<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

use App\Services\ProductLogic;

use DB;

class ProductController extends Controller
{
    public function get_productlist()
    {

        try {
            $productlist = DB::table('products')
            ->get();
            return response()->json($productlist, 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => ['code' => 'product-001', 'message' => 'Product not found!']
            ], 404);
        }
        
    }
    public function get_searched_products(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => Helpers::error_processor($validator)], 403);
        }

        $products = ProductLogic::search_products($request['name']);
        return response()->json($products, 200);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //
        $products = Product::get();
        return view('product/list', ['products' => $products]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //
        $products = Product::get();
        return view('product/list', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required'
        ];
        $request->validate($rules);
        $data = $request->all();

       

        if ($request->hasFile('image')) {
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $destinationPath = public_path() . '/uploads/';
            $request->file('image')->move($destinationPath, $fileNameToStore);

            Product::insert([
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->description,
                'image' => 'uploads/' . $fileNameToStore,
            ]);
        }

        return redirect()->route('products')->with('Product added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
        $product = Product::where('id', $id)->get();

        return view('product/edit', ['product' => $product[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();
        unset($data['_method']);
        unset($data['_token']);

        if ($request->hasFile('image')) {
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = time() . '.' . $extension;
            $destinationPath = public_path() . '/uploads/';
            $request->file('image')->move($destinationPath, $fileNameToStore);
            $data['image'] = 'uploads/' . $fileNameToStore;
        }
        $update = Product::where('id', $id)->update($data);
        if ($update) {
            return redirect()->route('products')->with('warning','Product updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $Product = Product::findOrFail($id)->delete();
        return redirect()->route('products')->with('Product deleted successfully');
    }
}
