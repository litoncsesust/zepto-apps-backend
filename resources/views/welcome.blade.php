@extends('layouts.app')
@section('content')
  @if (Route::has('login'))
    @auth
    <a href="{{ url('/') }}">Dashboard</a>
    @else
      @include('layouts.partials.login-form')
    @endauth
  @endif
@endsection
