@include('layouts.partials.head')
@if (Auth::check())
  @include('layouts.partials.top-navbar')
@endif


    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-edit"></i> {{trans('product.product')}} {{trans('product.update')}}</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{route('update', $product['id'])}}" method="post" id="product_form"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.name')}}</label>
                        <input type="text" name="name" value="{{$product['name']}}" class="form-control"
                               placeholder="New Product" required>
                    </div>

               
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.price')}}</label>
                        <input type="number" value="{{$product['price']}}" min="0" max="100000" name="price"
                                class="form-control" step="0.01"
                                placeholder="Ex : 100" required>
                    </div>

                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.description')}}</label>
                        <textarea type="text" name="description" class="form-control">{{$product['description']}}</textarea>
                    </div>

                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.image')}}</label>
                        <input type="file" name="image" class="form-control" placeholder="Image" required>
                    </div>
                    <div class="form-group">
                        <h4 class="input-label" for="exampleFormControlInput1">{{trans('product.current')}} {{trans('product.product')}} {{trans('product.image')}}</h4>
                        <div class="profile-picture" style="background-image: url({{asset($product['image'])}})"></div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">{{trans('product.update')}}</button>
                </form>
            </div>
        </div>
    </div>



