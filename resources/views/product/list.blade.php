@include('layouts.partials.head')
@if (Auth::check())
  @include('layouts.partials.top-navbar')
@endif

<div class="content container-fluid">
   
    <div class="row justify-content-center">
        <div class="pt-5 col-sm-10 col-md-10">
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col-sm mb-2 mb-sm-0">
                        <h1 class="page-header-title"><i class="tio-filter-list"></i> {{trans('product.product')}} {{trans('product.list')}}</h1>
                    </div>
                </div>
            </div>
            <table id="sortTable" class="mt-4 table table-striped table-hover">
                <thead class="case-upper top-border bottom-border">
                    <th>{{trans('product.name')}}</th>
                    <th>{{trans('product.price')}}</th>
                    <th>{{trans('product.description')}}</th>
                    <th>{{trans('product.image')}}</th>
                    <th class="text-right">{{trans('product.action')}}</th>
                </thead>
                <tbody>
                    @if($products)
                    @foreach($products as $key => $product)
                    <tr>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['price']}}</td>
                    <td>{{$product['description']}}</td>
                    <td><div class="profile-picture" style="background-image: url({{asset($product['image'])}})"></div></td>
                    <td class="text-right">
                        <a class="btn btn-sm btn-primary" href="{{route('edit',$product['id'])}}"><span data-feather="edit"></span></a>
                        <a class="btn btn-sm btn-danger" href="{{route('destroy', $product['id'])}}" onclick="event.preventDefault(); document.getElementById('table_delete_{{ $key+1 }}').submit();"><span data-feather="trash-2"></span></i></a>
                        <form id="table_delete_{{ $key+1 }}" action="{{route('destroy', $product['id'])}}" method="POST" style="display: none;">
                        <input name="_method" type="hidden" value="DELETE">
                        {{ csrf_field() }}
                        <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                        </form>
                    </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

