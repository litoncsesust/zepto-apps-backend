@include('layouts.partials.head')
@if (Auth::check())
  @include('layouts.partials.top-navbar')
@endif

<div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-add-circle-outlined"></i> {{trans('messages.add')}} {{trans('messages.new')}} {{trans('messages.product')}}</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{route('store')}}" method="post" id="product_form"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.name')}}</label>
                        <input type="text" name="name" class="form-control" placeholder="New Product" required>
                    </div>
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.price')}}</label>
                        <input type="number" min="0" max="100000" step="0.01" value="0" name="price" class="form-control"
                                placeholder="Ex : 100" required>
                    </div>                      

                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.description')}}</label>
                        <textarea type="text" name="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('product.image')}}</label>
                        <input type="file" name="image" class="form-control" placeholder="Image" required>
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-primary">{{trans('product.submit')}}</button>
                </form>
            </div>
        </div>
    </div>




@include('layouts.partials.footer')
