<?php

use Illuminate\Support\Facades\Route;
$currentPath = Route::getFacadeRoot()->current()->uri();
?>
  <!-- Scripts -->
  <script>
    var projectPath = "<?php echo URL::to('/'); ?>";
  </script>
  <!-- Bootstrap core JavaScript-->
  <!-- <script src="{{ asset('vendor/bootstrap/4.3.1/js/jquery-3.3.1.slim.min.js') }}"></script> -->
  <script src="{{ asset('vendor/bootstrap/4.3.1/js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/4.3.1/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/4.3.1/js/popper.min.js') }}"></script>
  <script src="{{ asset('vendor/library/js/feather.min.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- Core plugin JavaScript-->

  <!-- Custom scripts for all pages-->
 	@if($currentPath =='home')
 <!-- Page level plugins -->
  <!-- <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script> -->

  <!-- Page level custom scripts -->
  <!-- <script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script> -->
  @endif
