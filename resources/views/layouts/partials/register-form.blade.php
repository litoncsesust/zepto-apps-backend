<form class="form-signin shadow v-middle" method="POST" action="{{ route('register') }}">
	@csrf
	<div class="text-center mb-4">
		<a href="{{ url('/') }}" class="logo mb-4 contain">&nbsp;</a>
		<h1 class="h2 mb-3">Conversational Commerce!</h1>
		<h3 class="mb-3">{{ __('login.btn_register_label') }}</h3>
		@if (session('warning'))
      <div class="alert alert-warning">
          {{ session('warning') }}
      </div>
    @endif
	</div>
	<div class="form-label-group">
		<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="{{ __('Name') }}" autofocus>
		<label for="name">{{ __('Name') }}</label>
		@error('name')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('Email') }}">
		<label for="email">{{ __('E-Mail Address') }}</label>
		@error('email')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">
		<label for="password">{{ __('Password') }}</label>
		@error('password')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Password confirmation') }}">
		<label for="password-confirm">{{ __('Confirm Password') }}</label>
	</div>
	<div class="form-label-group">
		<button type="submit" class="btn btn-primary btn-block showloader">{{ __('Register') }}</button>
	</div>
	<div class="form-label-group text-center bold">
		OR
	</div>
	<div class="form-label-group btn-group btn-block" >
		<a href="{{ url('/') }}" class="btn btn-primary"><span data-feather="skip-back"></span> Back</a>
		<a href="{{ url('auth/google') }}" class="btn btn-secondary showloader">Signup with Google</a>
	</div>
</form>