<nav class="col-md-2 d-none d-md-block color-bg-corporate sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      @if(Auth::user()->role_id === 1 || Auth::user()->role_id === 999)
      <!-- <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}"><span data-feather="cpu"></span> Dashboard</a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="{{ url('channel-type') }}"><span data-feather="bar-chart-2"></span> {{ __('sidebar.btn_channel_type_label') }}</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="{{ url('/retail-solution-type') }}"><span data-feather="bar-chart-2"></span> {{ __('sidebar.btn_retail_solution_type_label') }}</a>
      </li> -->
      @endif
      @if(Auth::user()->role_id === 999)
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/retailer-list') }}"><span data-feather="users"></span> {{ __('sidebar.btn_retailer_list') }}</a>
      </li>
      @endif
      @if(in_array(Auth::user()->role_id, [1,2,999]))
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/retail-solution') }}"><span data-feather="sliders"></span> {{ __('sidebar.btn_retail_solution_label') }}</a>
      </li>
      @endif
      @if(Auth::user()->role_id === 1 || Auth::user()->role_id === 999)
      <!-- <li class="nav-item">
        <a class="nav-link" href="#"><span data-feather="users"></span> Inventory</a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="#"><span data-feather="settings"></span> {{ __('sidebar.btn_settings_label') }}</a>
      </li>
      @endif
    </ul>
  @if(Session::get('retailer-status') != 1)
    <div class="absolute alert alert-info activate text-center alert-dismissible fade show" role="alert">
      <h2>Activate your profile</h2>
      <a class="bold" href="{{ url('send-activation-email',[Auth::user()->email]) }}"><span data-feather="mail"></span> {{ __('Request for activation') }}</a>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
  @endif
  </div>
</nav>