<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Are you sure?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body text-center">Are you sure you want to delete <strong class="name"></strong>?</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal"><span data-feather="x"></span> {{ __('Cancel') }}</button>
        <form id="delete" action="" method="POST" class="no-margin no-padding">
          <input name="_method" type="hidden" value="DELETE">
          {{ csrf_field() }}
          <button class="btn btn-danger showloader" type="submit"><span data-feather="trash-2"></span> Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>