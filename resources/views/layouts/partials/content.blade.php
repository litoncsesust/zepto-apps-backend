@if (Auth::check())
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
@else
	<main role="main" class="col ml-sm-auto relative">
@endif
	<div class="h90-height justify-content-center">
		@yield('content')
	</div>
</main>
