<form class="form-signin shadow v-middle" method="POST" action="{{ route('login') }}">
	@csrf
	<div class="text-center mb-4">
		<a href="{{ url('/') }}" class="logo mb-4 contain">&nbsp;</a>
		<h1 class="h2 mb-3">{{ __('login.title') }}</h1>
		<h3 class="mb-3">Login</h3>
		@if (session('warning'))
      <div class="alert alert-warning">
          {{ session('warning') }}
      </div>
    @endif
	</div>
	<div class="form-label-group">
		<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}" autofocus>
		<label for="email">{{ __('login.email_label') }}</label>
		@error('email')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
		<label for="password">{{ __('login.password_label') }}</label>
		@error('password')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<button type="submit" class="showloader btn btn-primary btn-block"><span data-feather="log-in"></span> {{ __('login.btn_login_label') }}</button>
	</div>
</form>
