<nav class="navbar navbar-expand-md navbar-dark fixed-top color-bg-corporate p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0 bold" href="{{ url('/') }}">Zepto Apps!</a>
  <div class="collapse navbar-collapse float-right" id="navbarCollapse">
    <ul class="navbar-nav flex-row ml-md-auto d-md-flex">
      <li>
        <ul>
          <li class="nav-item dropdown">           
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="img-profile rounded-circle" src="http://test.bordingvista.com/dev.conversational-commerce/images/placeholder-profile.jpg">
              {{-- ProfileController::showuser(Auth::user()->id) --}}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item bold color-text-white" href="{{ route('profile.edit',[Auth::user()->id]) }}"><span data-feather="edit"></span> {{ __('topnav.btn_edit_profile_label') }}</a>
              <a class="dropdown-item bold color-text-white" href="{{ route('logout') }}" data-toggle="modal" data-target="#logoutModal"><span data-feather="log-out"></span> {{ __('topnav.btn_logout_label') }}</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>