<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" id="bootstrap" href="{{ asset('vendor/bootstrap/4.3.1/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" id="app" href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <title>Zepto Apps! - @yield('title')</title>
  </head>
  <?php //use Illuminate\Support\Facades\Route;use Illuminate\Support\Facades\URL;  ?>
  @if (Auth::check())
    @php
      $body_class = 'dashboard'
    @endphp
  @else
    @php
      $body_class = 'login'
    @endphp
  @endif
  <body class="{{$body_class}}">
