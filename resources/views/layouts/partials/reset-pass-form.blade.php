<form class="form-signin shadow v-middle" method="POST" action="{{ route('password.email') }}">
	@csrf
	<div class="text-center mb-4">
		<a href="{{ url('/') }}" class="logo mb-4 contain">&nbsp;</a>
		<h1 class="h2 mb-3">Conversational Commerce!</h1>
		<h3 class="mb-3">{{ __('Reset Password') }}</h3>
		@if (session('status'))
		<div class="alert alert-success" role="alert">
			{{ session('status') }}
		</div>
		@endif
	</div>
	<div class="form-label-group">
		<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail Address">
		<label for="email">{{ __('E-Mail Address') }}</label>
		@error('email')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
		@enderror
	</div>
	<div class="form-label-group">
		<button type="submit" class="btn btn-primary btn-block">{{ __('Send Password Reset Link') }}</button>
	</div>
</form>