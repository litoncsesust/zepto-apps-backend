@include('layouts.partials.head')
@if (Auth::check())
  @include('layouts.partials.top-navbar')
@endif
<div class="container-fluid">
  <div class="row">
    @if (Auth::check())
      @include('layouts.partials.sidebar')
    @endif
    @include('layouts.partials.content')
  </div>
</div>
@include('layouts.partials.footer')
