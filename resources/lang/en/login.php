<?php
return [
    "btn_login_label" => "Login",
    "email_label" => "E-Mail Address",
    "forgot_password_label" => "Forgot Your Password?",
    "password_label" => "Password",
    "remember_me_label" => "Remember Me",
    "email" => "Email Address",
    "password" => "Password",
    "login" => "Login",
    "title" => "Zepto Apps"
];
