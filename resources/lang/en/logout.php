<?php
return [
    "btn_logout_cancel_label" => "Cancel",
    "btn_logout_label" => "Log Out",
    "text" => "Click \"Log out\" below if you are ready to end your current session.",
    "title" => "Ready to leave?"
];
