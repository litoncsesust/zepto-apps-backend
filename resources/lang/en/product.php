
<?php

return [
    "title" => "Zepto Apps!", 
    "welcome" => "",
    'admin' => 'Admin',
    'sign_in' => 'Sign in',
    'want' => 'Want',
    'to' => 'to',
    'login' => 'login',
    'your' => 'your',
    'branches' => 'branches',
    'branch' => 'branch',
    'email' => 'Email',
    'password' => 'Password',
    'remember' => 'Remember',
    'me' => 'me',
    'dashboard' => 'Dashboard',
    'product' => 'Product',
    'add' => 'Add',
    'new' => 'New',
    'list' => 'List',
    'welcome' => 'Welcome',
    'welcome_message' => 'Hello, here is what happening with your restaurant today.',
    'products' => 'Products',
    'total' => 'Total',
    
    'footer' => 'Footer',
    'text' => 'Text',
    'image' => 'Image',
    'description' => 'Description',
    'name' => 'Title',
    'price' => 'Price',
    'current' => 'Current',
    'update' => 'Update',
    'action' => 'Action',
    'submit' => 'Submit'
    
];

