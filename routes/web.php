<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();


Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

//Route::group(['middleware' => ['auth:web','checkAdmin']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/products', [App\Http\Controllers\ProductController::class, 'index'])->name('products');
    Route::post('store', [App\Http\Controllers\ProductController::class, 'store'])->name('store');
    Route::get('edit/{id}', [App\Http\Controllers\ProductController::class, 'edit'])->name('edit');
    Route::post('update/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('update');
    Route::delete('destroy/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('destroy');    
    Route::get('search', 'ProductController@get_searched_products');
//});

